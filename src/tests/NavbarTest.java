package tests;

import base.TestCases;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NavbarTest extends TestCases {

    /**
     * The driver to user the browser.
     */
    private WebDriver driver;

    /**
     * This method initialize the driver.
     */
    @BeforeClass
    public void setUp() {
        driver = getDriver();
    }

//    /**
//     * This test validates the redirecting on home link.
//     */
//    @Test
//    public void logoImage(){
//        getNavigationBar().clickOnLogoImage();
//    }
//
//    /**
//     * This test validates the redirecting on home link.
//     */
//    @Test
//    public void goToHome(){
//        getNavigationBar().clickOnHomeLink();
//    }
//
//    /**
//     * This test validates the redirecting on login link.
//     */
//    @Test
//    public void goToLogin(){
//        getNavigationBar().clickOnLogin();
//    }
//
//    /**
//     * This test validates the redirecting on register link.
//     */
//    @Test
//    public void goToRegister(){
//        getNavigationBar().clickOnSignUp();
//    }
//
    @Test
    public void selectCoin(){
        getNavigationBar().selectCoin();
    }

    @Test
    public void selectLanguage() {
        getNavigationBar().selectLanguage();
    }
}
