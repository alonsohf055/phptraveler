package tests;

import base.TestCases;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_objects.RegisterPage;

public class RegisterTest extends TestCases {

    /**
     * The driver to user the browser.
     */
    private WebDriver driver;

    /**
     * This method initialize the driver.
     */
    @BeforeClass
    public void setUp() {
        driver = getDriver();
    }

    /**
     * This method tries to register a new user with valid data.
     * @param name is the user name.
     * @param lastname is the user last name.
     * @param mobileNumber is the user mobile phone number.
     * @param email is the email address.
     * @param password is the user password.
     * @param confirmPassword is the confirmation password.
     */
    @Test(dataProvider = "registerCredentials", dataProviderClass = data.RegisterDataProviders.class)
    public void isUserRegistered(String name, String lastname, String mobileNumber, String email, String password, String confirmPassword) {
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.isUserRegistered(name, lastname, mobileNumber, email, password, confirmPassword);
    }

    /**
     * this method tries to register a new user with invalid data.
     * @param name is the user name.
     * @param lastname is the user last name.
     * @param mobileNumber is the user mobile phone number.
     * @param email is the email.
     * @param password is the password.
     * @param confirmPassword is the confirmation password.
     */
    @Test(dataProvider = "registerCredentials", dataProviderClass = data.RegisterDataProviders.class)
    public void attemptToRegister(String name, String lastname, String mobileNumber, String email, String password, String confirmPassword) {
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.registerAttempt(name, lastname, mobileNumber, email, password, confirmPassword);
    }
}