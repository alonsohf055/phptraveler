package data;

import org.testng.annotations.DataProvider;

public class LoginDataProviders {

    /**
     * This method send valid credentials to logged in.
     * @return valid credentials data.
     */
    @DataProvider (name = "loginAuthentication")
    public static Object[][] credentials(){
        return new Object[][] {
                {"", ""},
                {"",""}
        };
    }

    /**
     * This method send invalid data to try logged in.
     * @return invalid credentials data.
     */
    @DataProvider (name = "attemptsToLogin")
    public static Object[][] attempts(){
        return new Object[][] {
                {"", ""},
                {"",""}
        };
    }
}
