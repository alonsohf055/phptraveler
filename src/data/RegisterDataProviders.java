package data;

import org.testng.annotations.DataProvider;

public class RegisterDataProviders {

    /**
     * This method send valid credentials to register.
     * @return valid credentials data.
     */
    @DataProvider(name = "registerCredentials")
    public static Object[][] credentials(){
        return new Object[][] {
                {"Gabriel", "Duran", "87330886", "jduran@growthaccelerationpartners.com", "gapuser12345", "gapuser12345"}
        };
    }

    /**
     * This method send invalid data to try register.
     * @return invalid credentials data.
     */
    @DataProvider (name = "attemptsToRegister")
    public static Object[][] attempts(){
        return new Object[][] {
                {"Gabriel", "", "8754", "jduran@growthaccelerationpartners.com", "gapuser12345", "12345"},
        };
    }

}
