package page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {

    /**
     * Web driver to interact with the browser.
     */
    private WebDriver driver;

    /**
     * UI selectors.
     */
    @FindBy (name = "firstname") WebElement firstName;
    @FindBy (name = "lastname") WebElement lastName;
    @FindBy (name = "phone") WebElement mobileNumber;
    @FindBy (name = "email") WebElement email;
    @FindBy (name = "password") WebElement password;
    @FindBy (name = "confirmpassword") WebElement confirmPassword;
    @FindBy (linkText = "SIGN UP") WebElement signUpButton;

    /**
     * Constructor method.
     * @param driver is the initialized web driver
     */
    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * This method tries to create a new registration with invalid data.
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param mobileNumber is the user mobile number
     * @param emial is the user email
     * @param password is the user password
     * @param confirmPassword is the user confirmation password
     */
    public void registerAttempt(String firstName, String lastName, String mobileNumber,
                                String emial, String password, String confirmPassword){
        typeFirstName(firstName);
        typeLastName(lastName);
        typeMobileNumber(mobileNumber);
        typeEmail(emial);
        typePassword(password);
        typeConfirmPassword(confirmPassword);
        clickOnSignUpButton();
    }

    /**
     * This method creates a new registration.
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param mobileNumber is the user mobile number
     * @param emial is the user email
     * @param password is the user password
     * @param confirmPassword is the user confirmation password
     */
    public void isUserRegistered(String firstName, String lastName, String mobileNumber,
                                 String emial, String password, String confirmPassword){
        typeFirstName(firstName);
        typeLastName(lastName);
        typeMobileNumber(mobileNumber);
        typeEmail(emial);
        typePassword(password);
        typeConfirmPassword(confirmPassword);
//        clickOnSignUpButton();
    }

    /**
     * This method types the user first name.
     * @param uFirstName is the user first name.
     */
    private void typeFirstName(String uFirstName) {
        firstName.clear();
        firstName.sendKeys(uFirstName);
    }

    /**
     * This method types the user last name.
     * @param uLastName is the user last name.
     */
    private void typeLastName (String uLastName) {
        lastName.clear();
        lastName.sendKeys(uLastName);
    }

    /**
     * This method types the user mobile number.
     * @param uMobileNumber is the user mobile number.
     */
    private void typeMobileNumber (String uMobileNumber) {
        mobileNumber.clear();
        mobileNumber.sendKeys(uMobileNumber);
    }

    /**
     * This method types the user email.
     * @param uEmail is the user email.
     */
    private void typeEmail (String uEmail) {
        email.clear();
        email.sendKeys(uEmail);
    }

    /**
     * This method types the user password.
     * @param uPassword is the user password.
     */
    private void typePassword (String uPassword) {
        password.clear();
        password.sendKeys(uPassword);
    }

    /**
     * This method types the confirmation password.
     * @param uConfirmPassword is the confirmation password
     */
    private void typeConfirmPassword (String uConfirmPassword) {
        confirmPassword.clear();
        confirmPassword.sendKeys(uConfirmPassword);
    }

    /**
     * This method clicks on sign up button.
     */
    private void clickOnSignUpButton() {
        signUpButton.click();
    }
}
