package page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    /**
     * Web driver to interact with the browser.
     */
    private WebDriver driver;

    /**
     * UI selectors.
     */
    @FindBy(name = "username") WebElement email;
    @FindBy(name = "password") WebElement password;
    @FindBy(name = "remember") WebElement chkRemember;
    @FindBy(linkText = "LOGIN") WebElement btnLogin;
    @FindBy(linkText = "SIGN UP") WebElement btnSignUp;
    @FindBy(linkText = "FORGET PASSWORD") WebElement btnForgetPassword;

    /**
     * Constructor method
     * @param driver is the initialized web driver
     */
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * This method tries to login with incorrect data.
     * @param email is the invalid email account.
     * @param password is the invalid password.
     */
    public void attemptToLogin(String email, String password) {
        typeEmail(email);
        typePassword(password);
        clickOnLoginButton();
    }

    /**
     * This method tries to login with valid data.
     * @param email is a valid email account.
     * @param password is a valid password.
     */
    public void isLoggedIn(String email, String password) {
        typeEmail(email);
        typePassword(password);
        clickOnLoginButton();
    }

    /**
     * This method types the user email in the input.
     * @param uEmail is the user email.
     */
    private void typeEmail(String uEmail) {
        email.clear();
        email.sendKeys(uEmail);
    }

    /**
     * This method types the user password in the input.
     * @param uPassword is the user password.
     */
    private void typePassword(String uPassword){
        password.clear();
        password.sendKeys(uPassword);
    }

    /**
     * This method select the remember me option.
     */
    private void selectRememberOption() {
        chkRemember.click();
    }

    /**
     * This method clicks on login button.
     */
    private void clickOnLoginButton() {
        btnLogin.click();
    }

    /**
     * This method clicks on sign up button.
     */
    private void clickOnSignUpButton() {
        btnSignUp.click();
    }

    /**
     * This method click on forget password button.
     */
    private void clicksOnForgetPasswordButton() {
        btnForgetPassword.click();
    }
}
