package page_objects.navbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationBar {

    /**
     * Web driver to interact with the browser.
     */
    WebDriver driver;

    /**
     * UI selectors.
     */
    @FindBy(xpath = "//*[@class='navbar-brand']") WebElement logoImage;
    @FindBy(xpath = "//*[@id=\"collapse\"]/ul[1]/li[1]") WebElement homeLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[2]") WebElement hotelsLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[3]") WebElement flightsLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[4]") WebElement toursLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[5]") WebElement carsLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[6]") WebElement visaLink;
    @FindBy(linkText = "//*[@id=\"collapse\"]/ul[1]/li[7]") WebElement blogLink;
    @FindBy(id = "li_myaccount") WebElement myAccountLink;
    @FindBy(xpath = "//*[@id=\"collapse\"]/ul[2]/ul/li[2]") WebElement coin;
    @FindBy(xpath = "//*[@id=\"collapse\"]/ul[2]/ul/ul/li") WebElement language;

    /**
     * Constructor method.
     * @param driver is the initialized web driver
     */
    public NavigationBar(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * This method clicks on Logo Image on navigation bar.
     */
    private void clickOnLogoImage() {
        logoImage.click();
    }

    /**
     * This method clicks on Home link on navigation bar.
     */
    private void clickOnHomeLink() {
        homeLink.click();
    }

    /**
     * This method clicks on Hotels link on navigation bar.
     */
    private void clickOnHotelsLink(){
        hotelsLink.click();
    }

    /**
     * This method clicks on Flights link on navigation bar.
     */
    private void clickOnFlightsLink(){
        flightsLink.click();
    }

    /**
     * This method clicks on Tours link on navigation bar.
     */
    private void clickOnToursLink(){
        toursLink.click();
    }

    /**
     * This method clicks on Cars link on navigation bar.
     */
    private void clickOnCarsLink(){
        carsLink.click();
    }

    /**
     * This method clicks on Visa link on navigation bar.
     */
    private void clickOnVisaLink(){
        visaLink.click();
    }

    /**
     * This method clicks on Blog link on navigation bar.
     */
    private void clickOnBlogLink(){
        blogLink.click();
    }

    /**
     * This method clicks on Login link.
     */
    public void clickOnLogin() {
        myAccountLink.click();
        WebElement loginLink = driver.findElement(By.linkText("Login"));
        loginLink.click();
    }

    /**
     * This method clicks on Sign Up link.
     */
    public void clickOnSignUp() {
        myAccountLink.click();
        WebElement signupLink = driver.findElement(By.linkText("Sign Up"));
        signupLink.click();
    }

    /**
     * This method selects the coin type.
     */
    public void selectCoin(){
        Actions action = new Actions(driver);
        action.moveToElement(coin).perform();
        WebElement coinOption = driver.findElement(By.linkText("EUR"));
        action.perform();
        coinOption.click();
    }

    /**
     * This method selects the language.
     */
    public void selectLanguage(){
        Actions action = new Actions(driver);
        action.moveToElement(language).perform();
        WebElement languageOption = driver.findElement(By.linkText("Spanish"));
        action.perform();
        languageOption.click();
    }
}
