package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import page_objects.LoginPage;
import page_objects.NavigationBar;
import page_objects.RegisterPage;

import java.util.concurrent.TimeUnit;

public class TestCases {

    /**
     * Variable of the web driver.
     */
    private static WebDriver driver;

    /**
     * This method gets the current driver
     * @return the current driver.
     */
    public static WebDriver getDriver() {
        return driver;
    }

    /**
     * This method set the web driver configuration.
     * @param browser is the used browser.
     * @param baseUrl is the url to access.
     * @throws Exception when error.
     */
    public void setDriver(String browser, String baseUrl)
            throws Exception{
        if (browser.equalsIgnoreCase("firefox")) {
            driver = initFirefoxDriver(baseUrl);
        } else if (browser.equalsIgnoreCase("chrome")){
            driver = initChromeDriver(baseUrl);
        }
        else {
            throw new Exception("Browser is not correct");
        }
    }

    /**
     * This method initialize the firefox driver.
     * @param baseUrl is the url to access.
     * @return the firefox web driver.
     */
    private static WebDriver initFirefoxDriver(String baseUrl) {
        System.out.println("Browser Session Started");
        System.setProperty("webdriver.chrome.driver", "resources\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.get(baseUrl); //driver.navigate().to(baseUrl);
        return driver;
    }

    /**
     * This method initialize the chrome driver.
     * @param baseUrl is the url to access.
     * @return the chrome web driver.
     */
    private static WebDriver initChromeDriver(String baseUrl) {
        System.out.println("Browser Session Started");
        System.setProperty("webdriver.chrome.driver", "resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.get(baseUrl); //driver.navigate().to(baseUrl);
        return driver;
    }

    /**
     * This method sets up the web driver.
     * @param browser is the browser to use.
     * @param baseUrl is the url to access.
     */
    @Parameters({"browser", "baseUrl"})
    @BeforeClass
    public void setUp(String browser, String baseUrl) {
        try {
            setDriver(browser, baseUrl);
        } catch (Exception e) {
            System.out.println("Error... " + e.getMessage());
        }
    }

    /**
     * This method tears down the driver.
     */
    @AfterClass
    public void tearDown() {
        System.out.println("Browser Session Ended");
        driver.close();
    }

    /**
     * This method return a new instance of Navigation Bar Page.
     * @return the instance.
     */
    protected NavigationBar getNavigationBar(){
        return new NavigationBar(driver);
    }

    /**
     * This method return a new instance of Login Page.
     * @return the instance.
     */
    protected LoginPage getLoginPage(){
        return new LoginPage(driver);
    }

    /**
     * This method return a new instance of Register Page.
     * @return the instance.
     */
    protected RegisterPage getRegisterPage(){
        return new RegisterPage(driver);
    }

}
